﻿using ClosedXML.Excel;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToExcelFile = @"C:\Users\f.majidi\Desktop\Report.xlsx";

            //var psr = new List<Psr>();

            //////////////////////////////////////////////////////////////////Linq To Excel
            //var excelFile = new ExcelQueryFactory(pathToExcelFile);
            //string SheetName = excelFile.GetWorksheetNames().FirstOrDefault();
            //var sheetColumnName = excelFile.GetColumnNames(SheetName);
            //var getData = (from excel in excelFile.WorksheetRange("Line", "REMARK", SheetName) select excel).ToList();

            //foreach (var item in getData)
            //{
            //    psr.Add(new Psr
            //    {
            //        Ticketnumber = item["TicketNo"],
            //        Fare = int.Parse(item["FareAmount"]),
            //        SubmitDateTime = DateTime.Parse(item["Date"]),
            //        Tax = int.Parse(item["TAX"]),
            //        Commission = int.Parse(item["Commission"])
            //    });
            //}


            var psr = new List<Psr>();

            var wb = new XLWorkbook(pathToExcelFile);
            var ws = wb.Worksheet("Page 1");

            var headerRow = ws.RowsUsed(r => r.FirstCellUsed().GetString() == "Line" && r.LastCellUsed().GetString() == "REMARK").FirstOrDefault();

            var ticketRow = headerRow.LastCellUsed(f => f.GetString() == "TKT NBR").Address.ColumnNumber;
            var fareRow = headerRow.LastCellUsed(f => f.GetString() == "FARE").Address.ColumnNumber;
            var taxRow = headerRow.LastCellUsed(f => f.GetString() == "TAX").Address.ColumnNumber;
            var commisionRow = headerRow.LastCellUsed(f => f.GetString() == "COMMISIONE").Address.ColumnNumber;

            var dataRow = headerRow.RowBelow();
            do
            {
                var ticket = dataRow.Cell(ticketRow).Value.ToString();
                var fare = int.Parse(dataRow.Cell(fareRow).Value.ToString().Replace(",", ""));
                var tax = int.Parse(dataRow.Cell(taxRow).Value.ToString().Replace(",", ""));
                var commision = int.Parse(dataRow.Cell(commisionRow).Value.ToString().Replace(",", ""));

                psr.Add(new Psr
                {
                    Ticketnumber = ticket,
                    Fare = fare,
                    Tax = tax,
                    Commission = commision
                });

                dataRow = dataRow.RowBelow();
            }
            while (!dataRow.Cell(ticketRow).IsEmpty());


            ////////////////////////////////////////////////////////////////ClosedXML
        //    DataTable p = new DataTable();
        //    p.Columns.Add("TicketNumber");
        //    p.Columns.Add("Passenger");
        //    p.Columns.Add("Issue date", typeof(DateTime));
        //    p.Columns.Add("Fare", typeof(double));
        //    p.Columns.Add("Tax", typeof(double));
        //    p.Columns.Add("KU", typeof(double));
        //    p.Columns.Add("LP", typeof(double));
        //    p.Columns.Add("Commision", typeof(double));
        //    p.Columns.Add("Total", typeof(double));
        //    p.Columns.Add("FlightDate");
        //    p.Columns.Add("Route");
        //    p.Columns.Add("FlightNumber");
        //    p.Columns.Add("PNR");
        //    p.Columns.Add("PNR Total", typeof(double));
        //    List<string> _ret = new List<string>();
        //    var wb = new XLWorkbook(filename);
        //    var wb_new = new XLWorkbook();
        //    var ws = wb.Worksheet("Table 1");
        //    var headerRow =
        //        ws.RowsUsed(r => r.FirstCellUsed().GetString() == "NO" && r.LastCellUsed().GetString() == "CUR").FirstOrDefault();
        //    var ticketRow = headerRow.LastCellUsed(f => f.GetString() == "TicketCode").Address.ColumnNumber;
        //    var routeRow = headerRow.LastCellUsed(f => f.GetString() == "Route").Address.ColumnNumber;
        //    var paxRow = headerRow.LastCellUsed(f => f.GetString() == "PassengerName").Address.ColumnNumber;
        //    var issueRow = headerRow.LastCellUsed(f => f.GetString() == "Issue Date").Address.ColumnNumber;
        //    var fareRow = headerRow.LastCellUsed(f => f.GetString() == "Fare").Address.ColumnNumber;
        //    var taxRow = headerRow.LastCellUsed(f => f.GetString().Trim() == "Tax").Address.ColumnNumber;
        //    var commisionRow = headerRow.LastCellUsed(f => f.GetString() == "Commision").Address.ColumnNumber;
        //    var totalRow = headerRow.LastCellUsed(f => f.GetString() == "Total Price").Address.ColumnNumber;
        //    using (var rows = ws.RowsUsed(r => r.LastCellUsed().GetString() == "IRR"))
        //    {
        //        foreach (var row in rows)
        //        {

        //            if (string.IsNullOrEmpty(row.Cell(ticketRow).GetString()))
        //            {
        //                continue;
        //            }
        //            var tax = GrabTax(row.Cell(taxRow).GetString());
        //            p.Rows.Add(
        //                row.Cell(ticketRow).GetString(), // ticket number
        //                row.Cell(paxRow).GetString(), // passenger name
        //                row.Cell(issueRow).GetDateTime(), // issue date
        //                row.Cell(fareRow).GetDouble(), // Fare
        //                tax.Item1, // Tax
        //                tax.Item2, // KU
        //                tax.Item3, // LP
        //                row.Cell(commisionRow).GetDouble(), // Commision
        //                row.Cell(totalRow).GetDouble(), // Total
        //                null,
        //                row.Cell(routeRow).GetString(),
        //                null,
        //                null,
        //                0
        //                );
        //        }
        //    }
        //    wb_new.AddWorksheet(p, "Sheet1");
        //    var new_file = Path.GetFileNameWithoutExtension(filename) + "_converted";
        //    var directory = Path.GetDirectoryName(filename);
        //    var extension = Path.GetExtension(filename);
        //    wb_new.SaveAs(Path.Combine(directory, new_file + "." + extension));
        //}
        //private static Tuple<double, double, double> GrabTax(string taxString)
        //{
        //    var TaxPattern = "(?:\\d*\\.)?\\d+";
        //    var matches = Regex.Matches(taxString, TaxPattern);
        //    if (matches.Count == 3)
        //    {
        //        var tax = double.Parse(matches[0].Value);
        //        var ku = double.Parse(matches[1].Value);
        //        var LP = double.Parse(matches[2].Value);
        //        return new Tuple<double, double, double>(tax, ku, LP);
        //    }
        //    return new Tuple<double, double, double>(0, 0, 0);
        //}

    }
}
