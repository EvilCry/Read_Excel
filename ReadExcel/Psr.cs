﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadExcel
{
    public class Psr
    {
        public Psr()
        {

        }
        public long Id { get; set; }
        public DateTime SubmitDateTime { get; set; }
        public string Airline { get; set; }
        public string Ticketnumber { get; set; }
        public bool Isinternational { get; set; }
        public int Fare { get; set; }
        public int Tax { get; set; }
        public int Ku { get; set; }
        public int Lp { get; set; }
        public string Commissionrate { get; set; }
        public int Commission { get; set; }
        public int Totalprice { get; set; }
        public DateTime Createdon { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
